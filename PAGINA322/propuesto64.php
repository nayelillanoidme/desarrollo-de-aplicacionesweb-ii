<!DOCTYPE html>
<html>
<head>
	<title>Determinar si una letra está en mayúscula o minúscula</title>
</head>
<body>
	<h1>Determinar si una letra está en mayúscula o minúscula</h1>
	<form>
		<label for="letra">Ingrese una letra:</label>
		<input type="text" id="letra" name="letra"><br><br>
		<button type="button" onclick="determinarCase()">Determinar case</button><br><br>
		<label for="resultado">La letra está en:</label>
		<input type="text" id="resultado" name="resultado" readonly>
	</form>
	<script>
		function determinarCase() {
			let letra = document.getElementById("letra").value;
			let resultado = "";
			if (letra === letra.toUpperCase()) {
				resultado = "mayúscula";
			} else if (letra === letra.toLowerCase()) {
				resultado = "minúscula";
			} else {
				resultado = "No es una letra";
			}
			document.getElementById("resultado").value = resultado;
		}
	</script>
</body>
</html>
