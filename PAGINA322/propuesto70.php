<!DOCTYPE html>
<html>
<head>
	<title>Desencriptar frase</title>
</head>
<body>
	<h1>Desencriptar frase</h1>
	<form>
		<label for="frase">Ingrese la frase encriptada:</label>
		<input type="text" id="frase" name="frase"><br><br>
		<button type="button" onclick="desencriptar()">Desencriptar frase</button><br><br>
		<label for="resultado">La frase desencriptada es:</label>
		<input type="text" id="resultado" name="resultado" readonly>
	</form>
	<script>
		function desencriptar() {
			let frase = document.getElementById("frase").value.toLowerCase();
			let desencriptada = "";
			for (let i = 0; i < frase.length; i++) {
				let letra = frase.charAt(i);
				if (letra >= "a" && letra <= "z") {
					let codigo = letra.charCodeAt(0) - 3;
					if (codigo < 97) {
						codigo += 26;
					}
					desencriptada += String.fromCharCode(codigo);
				} else {
					desencriptada += letra;
				}
			}
			document.getElementById("resultado").value = desencriptada;
		}
	</script>
</body>
</html>
