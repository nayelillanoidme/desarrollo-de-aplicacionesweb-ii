<?php
//Variables
$lado = 5;
$perimetro = $lado * 4;
$area = $lado * $lado;
?>

<html>

<head>
    <title>Área y perímetro de un cuadrado</title>
    <style type="text/css">
        body {
            font-family: "Times New Roman",Courier,serif;
            background-color:;
        }
        
        table {
            border-collapse: collapse;
            margin: 5 auto;
            background-color:;
            border: 8px solid #008080;
            color:  ;
        }
        
        table td {
            border: 40px solid ;
            padding: 15px;
        }
        
        table th {
            background:#E6E6FA;
            color:#663399 ;
            border: 15px solid #008080;
            padding: 15px;
        }
        
        .TextoFondo {
            background-color:#00CED1;
        }

    </style>
</head>

<body>
    <table width="241" border="0">
        <tr>
            <th colspan="2">Área y perímetro de un cuadrado</th>
        </tr>
        <tr>
            <td>Lado:</td>
            <td><?= $lado ?></td>
        </tr>
        <tr>
            <td>Perímetro:</td>
            <td><?= $perimetro ?></td>
        </tr>
        <tr>
            <td>Área:</td>
            <td><?= $area ?></td>
        </tr>
    </table>
</body>

</html>
