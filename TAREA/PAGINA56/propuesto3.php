<?php
// Definir las variables iniciales
$mm = 0;
$m = 0;
$dm = 0;
$cm = 0;
$mm_resto = 0;

// Verificar si se recibió una cantidad en milímetros desde el formulario
if (isset($_POST['mm'])) {
  // Obtener la cantidad en milímetros y convertirla a entero
  $mm = (int)$_POST['mm'];

  // Realizar la conversión
  $m = floor($mm / 1000);
  $dm = floor(($mm % 1000) / 100);
  $cm = floor(($mm % 100) / 10);
  $mm_resto = $mm % 10;
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Conversión de Milímetros</title>
  <meta charset="UTF-8">
  <style type="text/css">
    body {
      font-family: Arial, sans-serif;
    }
    label, input, button {
      font-size: 1.2rem;
      padding: 0.5rem;
      margin: 0.5rem;
    }
    button {
      background-color: #4CAF50;
      border: none;
      color: white;
      padding: 0.5rem 1rem;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 1.2rem;
      margin: 0.5rem;
      cursor: pointer;
    }
    .result {
      font-size: 1.5rem;
      font-weight: bold;
      margin: 1rem;
    }
  </style>
</head>
<body>
  <h1>Conversión de Milímetros</h1>
  <form method="POST">
    <label for="mm">Cantidad en milímetros:</label>
    <input type="number" id="mm" name="mm" value="<?php echo $mm; ?>" required>
    <button type="submit">Convertir</button>
  </form>
  <?php if ($mm > 0): ?>
    <div class="result">
      La conversión de <?php echo $mm; ?> mm es: <?php echo $m; ?> m <?php echo $dm; ?> dm <?php echo $cm; ?> cm <?php echo $mm_resto; ?> mm
    </div>
  <?php endif; ?>
</body>
</html>
