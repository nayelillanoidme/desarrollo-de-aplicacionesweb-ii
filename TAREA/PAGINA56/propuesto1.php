<!DOCTYPE html>
<html>
<head>
  <title>Suma y diferencia de dos números enteros</title>
</head>
<body>
  <h1>Suma y diferencia de dos números enteros</h1>
  <form method="POST" action="">
    <label for="num1">Primer número:</label>
    <input type="number" id="num1" name="num1" required>
    <br>
    <label for="num2">Segundo número:</label>
    <input type="number" id="num2" name="num2" required>
    <br>
    <input type="submit" value="Calcular">
  </form>
  
  <?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      $a = $_POST['num1']; // primer número entero
      $b = $_POST['num2']; // segundo número entero

      // encontrar la suma de a y b
      $suma = $a + $b;

      // encontrar la diferencia de a y b
      $diferencia = $a - $b;

      // Mostrar los resultados en la página
      echo "La suma de $a y $b es: $suma <br>";
      echo "La diferencia de $a y $b es: $diferencia";
    }
  ?>
</body>
</html>

