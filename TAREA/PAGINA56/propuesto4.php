<?php
if (isset($_POST['btnCalcular'])) {
    // Obtener los valores ingresados por el usuario
    $num1 = (int)$_POST['num1'];
    $num2 = (int)$_POST['num2'];
    $num3 = (int)$_POST['num3'];
    $num4 = (int)$_POST['num4'];

    // Calcular la suma de los números
    $suma = $num1 + $num2 + $num3 + $num4;

    // Calcular el porcentaje de cada número en función a la suma
    $porcentaje1 = ($num1 / $suma) * 100;
    $porcentaje2 = ($num2 / $suma) * 100;
    $porcentaje3 = ($num3 / $suma) * 100;
    $porcentaje4 = ($num4 / $suma) * 100;
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Porcentaje de números</title>
</head>
<body>
    <form method="post">
        <label for="num1">Número 1:</label>
        <input type="number" id="num1" name="num1" required>

        <label for="num2">Número 2:</label>
        <input type="number" id="num2" name="num2" required>

        <label for="num3">Número 3:</label>
        <input type="number" id="num3" name="num3" required>

        <label for="num4">Número 4:</label>
        <input type="number" id="num4" name="num4" required>

        <button type="submit" name="btnCalcular">Calcular porcentajes</button>
    </form>

    <?php if (isset($_POST['btnCalcular'])): ?>
        <p>Porcentaje de cada número:</p>
        <ul>
            <li>Número 1: <?= $porcentaje1 ?>%</li>
            <li>Número 2: <?= $porcentaje2 ?>%</li>
            <li>Número 3: <?= $porcentaje3 ?>%</li>
            <li>Número 4: <?= $porcentaje4 ?>%</li>
        </ul>
    <?php endif; ?>
</body>
</html>
