<!DOCTYPE html>
<html>
<head>
	<title>Contar números enteros</title>
</head>
<body>
	<h1>Contar números enteros entre dos números</h1>
	<label>Ingrese el primer número: <input type="number" id="num1"></label><br>
	<label>Ingrese el segundo número: <input type="number" id="num2"></label><br>
	<button onclick="contar()">Contar</button>
	<p id="resultado"></p>
	<script>
		function contar() {
			var num1 = parseInt(document.getElementById("num1").value);
			var num2 = parseInt(document.getElementById("num2").value);
			var cantidad = Math.abs(num1 - num2) + 1;
			document.getElementById("resultado").innerHTML = "Hay " + cantidad + " números enteros entre " + num1 + " y " + num2;
		}
	</script>
</body>
</html>
