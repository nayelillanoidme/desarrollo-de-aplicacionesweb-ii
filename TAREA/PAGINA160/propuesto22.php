<?php
// Definir un arreglo con los días de la semana
$diasSemana = array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");

// Verificar si se ha enviado el formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // Obtener el número del día de la semana
  $numDiaSemana = $_POST["numDiaSemana"];

  // Validar si el número ingresado es válido
  if ($numDiaSemana >= 1 && $numDiaSemana <= 7) {
    // Obtener el día de la semana correspondiente al número
    $diaSemana = $diasSemana[$numDiaSemana - 1];

    // Mostrar el resultado
    echo "El día de la semana correspondiente al número $numDiaSemana es: $diaSemana";
  } else {
    // Mostrar mensaje de error
    echo "El número ingresado no es válido. Debe ser un número del 1 al 7.";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Día de la semana</title>
</head>
<body>
	<form method="post">
		<label for="numDiaSemana">Ingrese un número del 1 al 7:</label>
		<input type="number" id="numDiaSemana" name="numDiaSemana" min="1" max="7">
		<input type="submit" value="Enviar">
	</form>
</body>
</html>
