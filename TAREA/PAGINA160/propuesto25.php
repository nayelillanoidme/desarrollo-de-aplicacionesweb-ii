<?php
if (isset($_POST["fecha"])) {
    $fecha = $_POST["fecha"];
    $segundos_faltantes = strtotime(date("Y-12-31")) - strtotime($fecha);
    $dias_faltantes = floor($segundos_faltantes / 86400);
    echo "Faltan $dias_faltantes días para que acabe el año.";
}
?>

<html>
<head>
    <title>Cálculo de días faltantes para el fin de año</title>
</head>
<body>
    <form method="post" action="x.php">
        <label>Ingrese una fecha:</label>
        <input type="date" name="fecha">
        <input type="submit" value="Calcular">
    </form>
</body>
</html>

