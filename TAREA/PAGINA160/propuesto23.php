<?php
// Verificar si se ha enviado el formulario
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  // Obtener el símbolo del operador
  $simbolo = $_POST["operador"];

  // Verificar el símbolo y devolver el nombre del operador correspondiente
  switch ($simbolo) {
    case "+":
      $nombreOperador = "suma";
      break;
    case "-":
      $nombreOperador = "resta";
      break;
    case "*":
      $nombreOperador = "multiplicación";
      break;
    case "/":
      $nombreOperador = "división";
      break;
    default:
      $nombreOperador = "operador inválido";
      break;
  }

  
  echo "El símbolo $simbolo corresponde al operador de $nombreOperador.";
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Nombre del operador aritmético</title>
</head>
<body>
	<form method="post">
		<label for="operador">Ingrese un operador aritmético (+, -, *, /):</label>
		<input type="text" id="operador" name="operador">
		<input type="submit" value="Enviar">
	</form>
</body>
</html>

