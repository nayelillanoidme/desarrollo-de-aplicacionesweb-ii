<?php
// Array que contiene los canales de televisión y sus números correspondientes
$canales = array(
  "1" => "dibujos",
  "2" => "deprtes",
  "3" => "comedia",
  "4" => "doramas",
  "5" => "novelas",
  "6" => "noticias",
  "7" => "farandula",

);

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  $numeroCanal = $_POST["canal"];

  if (array_key_exists($numeroCanal, $canales)) {
    echo "El canal número $numeroCanal es: " . $canales[$numeroCanal];
  } else {
    echo "El canal número $numeroCanal no existe.";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Nombre del canal de televisión</title>
</head>
<body>
	<form method="post">
		<label for="canal">Ingrese el número del canal de televisión:</label>
		<input type="text" id="canal" name="canal">
		 <input type="submit" value="Enviar">
	</form>
</body>
</html>