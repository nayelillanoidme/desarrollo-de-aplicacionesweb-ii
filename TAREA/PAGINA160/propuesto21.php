<?php
//Variables
$meses = array("", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
$mes = 0;

if(isset($_POST["btnConvertir"])){
    $mes = (int)$_POST["txtMes"];
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Convertir número de mes a letras</title>
</head>
<body>
    <h1>Convertir número de mes a letras</h1>

    <form method="post" action="propuesto21.php">
        <label for="txtMes">Ingresa el número del mes (1-12):</label>
        <input type="number" id="txtMes" name="txtMes" min="1" max="12" value="<?= $mes ?>">
        <button type="submit" name="btnConvertir">Convertir</button>
    </form>

    <?php if($mes != 0): ?>
        <p>El mes correspondiente al número <?= $mes ?> es <?= $meses[$mes] ?>.</p>
    <?php endif; ?>
</body>
</html>


