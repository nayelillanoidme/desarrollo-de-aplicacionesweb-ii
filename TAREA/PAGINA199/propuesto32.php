<?php
if (isset($_POST["rango_inicial"]) && isset($_POST["rango_final"])) {
    $rango_inicial = $_POST["rango_inicial"];
    $rango_final = $_POST["rango_final"];
    $pares = 0;
    $impares = 0;
    for ($i = $rango_inicial; $i <= $rango_final; $i++) {
        if ($i % 5 == 0) {
            continue; // saltar los múltiplos de 5
        }
        if ($i % 2 == 0) {
            $pares++;
        } else {
            $impares++;
        }
    }
    echo "En el rango de $rango_inicial a $rango_final, hay $pares números pares y $impares números impares (sin considerar los múltiplos de 5)";
}
?>

<html>
<head>
    <title>Cantidad de números pares e impares en un rango</title>
</head>
<body>
    <form method="post" action="propuesto32.php">
        <label>Rango inicial:</label>
        <input type="number" name="rango_inicial">
        <br>
        <label>Rango final:</label>
        <input type="number" name="rango_final">
        <br>
        <input type="submit" value="Calcular">
    </form>
</body>
</html>
