<?php
if (isset($_POST["numero"])) {
    $numero = $_POST["numero"];
    $factorial = 1;
    for ($i = 1; $i <= $numero; $i++) {
        $factorial *= $i;
    }
    echo "El factorial de $numero es: $factorial";
}
?>

<html>
<head>
    <title>Cálculo del factorial de un número</title>
</head>
<body>
    <form method="post" action="propuesto31.php">
        <label>Ingrese un número:</label>
        <input type="number" name="numero" min="0">
        <input type="submit" value="Calcular">
    </form>
</body>
</html>
