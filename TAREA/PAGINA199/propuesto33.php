<?php
// Variables
$n = 0;
$suma = 0;
$producto = 1;
$i = 1;

// Obtener el valor de N desde el formulario
if (isset($_POST["btnCalcular"])) {
    $n = (int)$_POST["txtn1"];

    // Calcular la suma y el producto de los N primeros números naturales múltiplos de 3
    while ($n > 0) {
        if ($i % 3 == 0) {
            $suma += $i;
            $producto *= $i;
            $n--;
        }
        $i++;
    }
}
?>

<html>

<head>
    <title>Suma y producto de los N primeros múltiplos de 3</title>
    <style type="text/css">
        .TextoFondo {
            background-color: #CCFFFF;
        }

        body {
            font-family: "Times New Roman", Courier, serif;
            background-color:;
        }

        table {
            border-collapse: collapse;
            margin: 5 auto;
            background-color:;
            border: 8 px solid #008080;
            color: ;
        }

        table td {
            border: 40px solid;
            padding: 15 px;
        }

        table th {
            background: #E6E6FA;
            color: #663399;
            border: 15px solid #008080;
            padding: 15px;
        }

        .TextoFondo {
            background-color: #00CED1;
        }
    </style>
</head>

<body>
    <form method="post" action="propuesto33.php">
        <table width="241" border="0">
            <tr>
                <td colspan="2"><strong>Suma y producto de los N primeros múltiplos de 3</strong> </td>
            </tr>
            <tr>
                <td width="81">Ingrese N: </td>
                <td width="150">
                    <input name="txtn1" type="text" id="txtn1" value="<?= $n ?>" />
                </td>
            </tr>
            <tr>
                <td>Suma:</td>
                <td>
                    <input name="txts" type="text" class="TextoFondo" id="txts" value="<?= $suma ?>" />
                </td>
            </tr>
            <tr>
                <td>Producto:</td>
                <td>
                    <input name="txtp" type="text" class="TextoFondo" id="txtp" value="<?= $producto ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
