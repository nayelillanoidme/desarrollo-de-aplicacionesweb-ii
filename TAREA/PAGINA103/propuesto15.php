<?php

// función para ordenar los números de forma ascendente
function ordenar_ascendente($a, $b, $c) {
    $numeros = array($a, $b, $c);
    sort($numeros);
    return $numeros;
}

// función para ordenar los números de forma descendente
function ordenar_descendente($a, $b, $c) {
    $numeros = array($a, $b, $c);
    rsort($numeros);
    return $numeros;
}

// comprobamos si se ha enviado el formulario
if (isset($_POST["enviar"])) {
    // recogemos los valores ingresados por el usuario
    $num1 = (int)$_POST["num1"];
    $num2 = (int)$_POST["num2"];
    $num3 = (int)$_POST["num3"];
    
    // ordenamos los números de forma ascendente y descendente
    $numeros_ascendente = ordenar_ascendente($num1, $num2, $num3);
    $numeros_descendente = ordenar_descendente($num1, $num2, $num3);
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Ordenar números</title>
</head>
<body>
	<h1>Ordenar números</h1>
	<form method="post">
		<label for="num1">Número 1:</label>
		<input type="number" name="num1" id="num1" required>
		<br><br>
		<label for="num2">Número 2:</label>
		<input type="number" name="num2" id="num2" required>
		<br><br>
		<label for="num3">Número 3:</label>
		<input type="number" name="num3" id="num3" required>
		<br><br>
		<input type="submit" name="enviar" value="Ordenar">
	</form>

	<?php if (isset($_POST["enviar"])): ?>
		<h2>Orden ascendente:</h2>
		<p><?= $numeros_ascendente[0] ?>, <?= $numeros_ascendente[1] ?>, <?= $numeros_ascendente[2] ?></p>
		<h2>Orden descendente:</h2>
		<p><?= $numeros_descendente[0] ?>, <?= $numeros_descendente[1] ?>, <?= $numeros_descendente[2] ?></p>
	<?php endif; ?>
</body>
</html>
