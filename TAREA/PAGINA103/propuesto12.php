<?php
if (isset($_POST["btnCalcular"])) {
    $num1 = (int)$_POST["num1"];
    $num2 = (int)$_POST["num2"];
    $menor = $num1 < $num2 ? $num1 : $num2;
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Encontrar el número menor</title>
	<meta charset="UTF-8">
	<style type="text/css">
		/* Estilos para la tabla */
		table {
			border-collapse: collapse;
			margin: 5 auto;
			background-color: #CCFFFF;
			border: 8px solid #008080;
			color: black;
		}
		table td {
			border: 40px solid white;
			padding: 15px;
		}
		table th {
			background: #E6E6FA;
			color: #663399;
			border: 15px solid #008080;
			padding: 15px;
		}
	</style>
</head>
<body>
	<form method="post">
		<table>
			<tr>
				<th colspan="2">Encontrar el número menor</th>
			</tr>
			<tr>
				<td>Número 1:</td>
				<td><input type="number" name="num1"></td>
			</tr>
			<tr>
				<td>Número 2:</td>
				<td><input type="number" name="num2"></td>
			</tr>
			<tr>
				<td colspan="2"><button type="submit" name="btnCalcular">Encontrar menor</button></td>
			</tr>
			<?php if (isset($menor)): ?>
				<tr>
					<td>El número menor es:</td>
					<td><?= $menor ?></td>
				</tr>
			<?php endif; ?>
		</table>
	</form>
</body>
</html>

