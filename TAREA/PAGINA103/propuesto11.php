<?php
$edad = 0;
$resultado = '';

if(isset($_POST['enviar'])) {
  $edad = (int)$_POST['edad'];

  if($edad >= 18) {
    $resultado = 'Es mayor de edad';
  } else {
    $resultado = 'Es menor de edad';
  }
}
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Determinar si es mayor o menor de edad</title>
  </head>
  <body>
    <form method="post" action="">
      <label>Edad:</label>
      <input type="number" name="edad" required>
      <br><br>
      <input type="submit" name="enviar" value="Enviar">
    </form>
    
    <?php if($resultado != '') { ?>
    <p>Resultado: <?php echo $resultado; ?></p>
    <?php } ?>
  </body>
</html>
