<!DOCTYPE html>
<html>
<head>
	<title>Determinar cuántas palabras se repiten</title>
</head>
<body>
	<h1>Determinar cuántas palabras se repiten</h1>
	<form>
		<label for="frase">Ingrese una frase:</label>
		<input type="text" id="frase" name="frase"><br><br>
		<button type="button" onclick="contarPalabras()">Contar palabras repetidas</button><br><br>
		<label for="resultado">Se repiten las palabras:</label>
		<input type="text" id="resultado" name="resultado" readonly>
	</form>
	<script>
		function contarPalabras() {
			let frase = document.getElementById("frase").value.toLowerCase();
			let palabras = frase.split(" ");
			let contador = {};
			let resultado = "";
			for (let i = 0; i < palabras.length; i++) {
				if (contador[palabras[i]]) {
					contador[palabras[i]]++;
				} else {
					contador[palabras[i]] = 1;
				}
			}
			for (let palabra in contador) {
				if (contador[palabra] > 1) {
					resultado += palabra + " ";
				}
			}
			if (resultado === "") {
				resultado = "ninguna";
			}
			document.getElementById("resultado").value = resultado;
		}
	</script>
</body>
</html>
