<!DOCTYPE html>
<html>
<head>
	<title>Determinar si una palabra es palíndromo</title>
</head>
<body>
	<h1>Determinar si una palabra es palíndromo</h1>
	<form>
		<label for="palabra">Ingrese una palabra:</label>
		<input type="text" id="palabra" name="palabra"><br><br>
		<button type="button" onclick="esPalindromo()">Determinar si es palíndromo</button><br><br>
		<label for="resultado">La palabra es:</label>
		<input type="text" id="resultado" name="resultado" readonly>
	</form>
	<script>
		function esPalindromo() {
			let palabra = document.getElementById("palabra").value;
			let resultado = "";
			if (palabra === palabra.split("").reverse().join("")) {
				resultado = "palíndromo";
			} else {
				resultado = "no es palíndromo";
			}
			document.getElementById("resultado").value = resultado;
		}
	</script>
</body>
</html>
