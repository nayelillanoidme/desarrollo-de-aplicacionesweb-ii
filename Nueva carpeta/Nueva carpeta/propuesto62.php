<!DOCTYPE html>
<html>
<head>
	<title>Invertir nombre</title>
</head>
<body>
	<h1>Invertir nombre</h1>
	<form>
		<label for="nombre">Ingrese su nombre:</label>
		<input type="text" id="nombre" name="nombre"><br><br>
		<button type="button" onclick="invertirNombre()">Invertir nombre</button><br><br>
		<label for="nombreInvertido">Nombre invertido:</label>
		<input type="text" id="nombreInvertido" name="nombreInvertido" readonly>
	</form>
	<script>
		function invertirNombre() {
			let nombre = document.getElementById("nombre").value;
			let nombreInvertido = "";
			for (let i = nombre.length - 1; i >= 0; i--) {
				nombreInvertido += nombre.charAt(i);
			}
			document.getElementById("nombreInvertido").value = nombreInvertido;
		}
	</script>
</body>
</html>
