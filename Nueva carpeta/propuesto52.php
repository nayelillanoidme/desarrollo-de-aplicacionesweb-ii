<?php
//Variables
$numeros = [];
$mayor = 0;
$menor = 0;

if (isset($_POST["btnCalcular"])) {
    // Almacenar los números ingresados en un vector
    $numeros = $_POST["numeros"];

    // Encontrar el número mayor y menor del vector
    $mayor = max($numeros);
    $menor = min($numeros);
}
?>

<html>
<head>
    <title>Mayor y menor de 4 números</title>
    <style type="text/css">
        .TextoFondo {
            background-color: #CCFFFF;
        }
        
        body {
            font-family: "Times New Roman", Courier, serif;
            background-color:;
        }
        
        table {
            border-collapse: collapse;
            margin: 5 auto;
            background-color:;
            border: 8px solid #008080;
            color: #87CEFA;
        }
        
        table td {
            border: 40px solid;
            padding: 15px;
        }
        
        table th {
            background:#E6E6FA;
            color:#663399;
            border: 15px solid #008080;
            padding: 15px;
        }
        
        .TextoFondo {
            background-color:#00CED1;
        }
    </style>
</head>

<body>
    <form method="post" action="propuesto54.php">
        <table width="241" border="0">
            <tr>
                <td colspan="2"><strong>Mayor y menor de 4 números</strong></td>
            </tr>
            <tr>
                <td>Número 1:</td>
                <td>
                    <input name="numeros[]" type="number" id="num1" value="<?= isset($numeros[0]) ? $numeros[0] : '' ?>" required />
                </td>
            </tr>
            <tr>
                <td>Número 2:</td>
                <td>
                    <input name="numeros[]" type="number" id="num2" value="<?= isset($numeros[1]) ? $numeros[1] : '' ?>" required />
                </td>
            </tr>
            <tr>
                <td>Número 3:</td>
                <td>
                    <input name="numeros[]" type="number" id="num3" value="<?= isset($numeros[2]) ? $numeros[2] : '' ?>" required />
                </td>
            </tr>
            <tr>
                <td>Número 4:</td>
                <td>
                    <input name="numeros[]" type="number" id="num4" value="<?= isset($numeros[3]) ? $numeros[3] : '' ?>" required />
                </td>
            </tr>
            <tr>
                
                <td>Mayor:</td>
                <td>
                    <input name="mayor" type="text" class="TextoFondo" id="mayor" value="<?= $mayor ?>" readonly />
                </td>
            </tr>
            <tr>
                <td>Menor:</td>
                <td>
                    <input name="menor" type="text" class="TextoFondo" id="menor" value="<?= $menor ?>" readonly />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
    </td>
</tr>
</table>
</form>
</body>

</html>
 
                    <