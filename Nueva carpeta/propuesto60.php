<!DOCTYPE html>
<html>
<head>
	<title>Matriz y números mayores</title>
</head>
<body>
	<form method="post" action="">
		<label for="filas">Ingrese el número de filas:</label>
		<input type="number" name="filas" id="filas">
		<br>
		<label for="columnas">Ingrese el número de columnas:</label>
		<input type="number" name="columnas" id="columnas">
		<br>
		<input type="submit" name="submit" value="Generar matriz">
	</form>
	<br>
	<?php
	if (isset($_POST['submit'])) {
		$filas = $_POST['filas'];
		$columnas = $_POST['columnas'];

		// Generar matriz aleatoria
		$matriz = array();
		for ($i=0; $i < $filas; $i++) { 
			for ($j=0; $j < $columnas; $j++) { 
				$matriz[$i][$j] = rand(0, 100);
			}
		}

		// Mostrar matriz
		echo "<table border='1'>";
		for ($i=0; $i < $filas; $i++) { 
			echo "<tr>";
			for ($j=0; $j < $columnas; $j++) { 
				echo "<td>" . $matriz[$i][$j] . "</td>";
			}
			echo "</tr>";
		}
		echo "</table>";

		// Obtener los números mayores de cada columna
		$maximos = array();
		for ($j=0; $j < $columnas; $j++) { 
			$maximo = $matriz[0][$j];
			for ($i=1; $i < $filas; $i++) { 
				if ($matriz[$i][$j] > $maximo) {
					$maximo = $matriz[$i][$j];
				}
			}
			$maximos[] = $maximo;
		}

		// Mostrar los números mayores de cada columna
		echo "<p>Los números mayores de cada columna son:</p>";
		echo "<ul>";
		foreach ($maximos as $maximo) {
			echo "<li>" . $maximo . "</li>";
		}
		echo "</ul>";
	}
	?>
</body>
</html>

