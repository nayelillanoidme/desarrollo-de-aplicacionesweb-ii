<?php
//Variables
$valor = 0;
$suma = 0;
$n = 0;
$i;

if (isset($_POST["btnCalcular"])) {
    $n = (int)$_POST["txtn1"];
    for($i = 0 ; $i < $n ; $i++ ){
        if($i % 3 == 0 && $i % 5 == 0) {
            $valor = $i;
            $suma +=  $valor;
        }
    }
    
    // Ordenar los números según la forma indicada
    $orden = $_POST["orden"];
    if ($orden == "A") {
        sort($_POST["numeros"]);
    } else {
        rsort($_POST["numeros"]);
    }
}

?>

<html>

<head>
    <title>Suma de divisibles de tres y cinco</title>
    <style type="text/css">
        .TextoFondo {
            background-color: #CCFFFF;
        }

        body {
            font-family: "Times New Roman",Courier,serif;
            background-color:;
        }

        table {
            border-collapse: collapse;
            margin: 5 auto;
            background-color:;
            border:8 px solid #008080;
            color: #00CED1 ;
        }

        table td {
            border: 40px solid ;
            padding:15 px;
        }

        table th {
            background:#E6E6FA;
            color:#663399 ;
            border: 15px solid #008080;
            padding: 15px;
        }

        .TextoFondo {
            background-color:#00CED1;
        }

    </style>
</head>

<body>
    <form method="post" action="x.php">
        <table width="241" border="0">
            <tr>
                <td colspan="2"><strong>ORDENAR 5 NUMEROS</strong> </td>
            </tr>
            <tr>
                
                <td>Ingresar los 5 números:</td>
                <td>
                    <?php for ($j=1; $j<=5; $j++): ?>
                        <input name="numeros[]" type="text" id="num<?= $j ?>" value="<?= $_POST['numeros'][$j-1] ?? '' ?>" />
                    <?php endfor; ?>
                </td>
            </tr>
            <tr>
                <td>Orden:</td>
                <td>
                    <select name="orden">
                        <option value="A">Ascendente</option>
                        <option value="D">Descendente</option>
                    </select>
                </td>
            </tr>
            <tr>
                
                <td>&nbsp;</td>
                <td>
                    <input name="ordenar" type="submit" id="ordenar" value="ordenar" />
                </td>
            </tr