<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $a = array(array($_POST['a11'], $_POST['a12']), array($_POST['a21'], $_POST['a22']));
    $b = array(array($_POST['b11'], $_POST['b12']), array($_POST['b21'], $_POST['b22']));
    $c = multiplicar_matrices($a, $b);
  }
  ?>
  <?php
  function multiplicar_matrices($a, $b) {
    $c = array();
    for ($i = 0; $i < 2; $i++) {
      for ($j = 0; $j < 2; $j++) {
        $c[$i][$j] = 0;
        for ($k = 0; $k < 2; $k++) {
          $c[$i][$j] += $a[$i][$k] * $b[$k][$j];
        }
      }
    }
    return $c;
  }
  
  ?>
  
  <!DOCTYPE html>
  <html>
  <head>
    <title>Multiplicación de matrices 2x2</title>
  </head>
  <body>
    <h1>Multiplicación de matrices 2x2</h1>
  
    <form method="post">
      <label>Matriz A:</label><br>
      <input type="number" name="a11" value="0"> <input type="number" name="a12" value="0"><br>
      <input type="number" name="a21" value="0"> <input type="number" name="a22" value="0"><br>
  
      <label>Matriz B:</label><br>
      <input type="number" name="b11" value="0"> <input type="number" name="b12" value="0"><br>
      <input type="number" name="b21" value="0"> <input type="number" name="b22" value="0"><br>
  
      <input type="submit" value="Calcular">
    </form>
  
    <?php if ($_SERVER['REQUEST_METHOD'] == 'POST') { ?>
      <h2>Resultado:</h2>
      <label>Matriz C:</label><br>
      <input type="number" name="c11" value="<?php echo $c[0][0]; ?>"> <input type="number" name="c12" value="<?php echo $c[0][1]; ?>"><br>
      <input type="number" name="c21" value="<?php echo $c[1][0]; ?>"> <input type="number" name="c22" value="<?php echo $c[1][1]; ?>"><br>
    <?php } ?>
  </body>
  </html>
