<?php
function sumaDigitosParesImpares($numero) {
    $sumaPares = 0;
    $sumaImpares = 0;
    
    
    $cadena = strval($numero);
    
    for ($i = 0; $i < strlen($cadena); $i++) {
        $digito = intval($cadena[$i]);
        if ($digito % 2 == 0) {
            $sumaPares += $digito;
        } else {
            $sumaImpares += $digito;
        }
    }
    
    
    return array($sumaPares, $sumaImpares);
}


if (isset($_POST['numero'])) {
    $numero = $_POST['numero'];
    $sumas = sumaDigitosParesImpares($numero);
    $sumaPares = $sumas[0];
    $sumaImpares = $sumas[1];
} else {
    
    $sumaPares = 0;
    $sumaImpares = 0;
}
?>

<html>
<head>
    <title>Suma de dígitos pares e impares</title>
    <style type="text/css">
<!--
.TextoFondo {
background-color:#CCFFFF;
}


 body {
            font-family: "Times New Roman",Courier,serif;
            background-color:#E6E6FA;
        }
        
        table {
            border-collapse: collapse;
            margin: 5 auto;
            background-color:#E6E6FA;
            border:8 px solid #008080;
            
            
        
            color: #20B2AA	;
        }
        
        table td {
            border: 40px solid ;
            padding:15 px;
        }
        
        table th {
            background:#E6E6FA;
            color:#663399 ;
            border: 15px solid #008080;
            padding: 15px;
        }
        
        .TextoFondo {
            background-color:#00CED1;
        }


-->
</style>
</head>
<body>
    <form method="post" action="x.php">
        <label for="numero">Ingrese un número:</label>
        <input type="text" name="numero" id="numero">
        <input type="submit" value="Calcular">
    </form>
    
    <?php if (isset($_POST['numero'])): ?>
    <p>Suma de dígitos pares: <?php echo $sumaPares ?></p>
    <p>Suma de dígitos impares: <?php echo $sumaImpares ?></p>
    <?php endif ?>
</body>
</html>
