<?php
// Función para obtener el promedio de las dos notas mayores
function obtenerPromedio($nota1, $nota2, $nota3) {
    $promedio = 0;
    if ($nota1 >= $nota2 && $nota1 >= $nota3) {
        $promedio = ($nota1 + max($nota2, $nota3)) / 2;
    } else if ($nota2 >= $nota1 && $nota2 >= $nota3) {
        $promedio = ($nota2 + max($nota1, $nota3)) / 2;
    } else {
        $promedio = ($nota3 + max($nota1, $nota2)) / 2;
    }
    return $promedio;
}

// Variables
$nota1 = 0;
$nota2 = 0;
$nota3 = 0;
$promedio = 0;

if (isset($_POST["btnCalcular"])) {
    $nota1 = (float)$_POST["txtn1"];
    $nota2 = (float)$_POST["txtn2"];
    $nota3 = (float)$_POST["txtn3"];
    $promedio = obtenerPromedio($nota1, $nota2, $nota3);
}
?>

<html>
<head>
    <title>Promedio de notas</title>
    <style type="text/css">
        <!--
.TextoFondo {
background-color:#CCFFFF;
}


 body {
            font-family: "Times New Roman",Courier,serif;
            background-color:#E6E6FA;
        }
        
        table {
            border-collapse: collapse;
            margin: 5 auto;
            background-color:#E6E6FA;
            border:8 px solid #008080;
            
            
        
            color: #5F9EA0	;
        }
        
        table td {
            border: 40px solid ;
            padding:15 px;
        }
        
        table th {
            background:#E6E6FA;
            color:#663399 ;
            border: 15px solid #008080;
            padding: 15px;
        }
        
        .TextoFondo {
            background-color:#00CED1;
        }


-->
      
    </style>
</head>

<body>
    <form method="post" action="propuesto72.php">
        <table>
            <tr>
                <td colspan="2"><strong>Promedio de notas</strong></td>
            </tr>
            <tr>
                <td>Nota 1:</td>
                <td><input name="txtn1" type="text" value="<?= $nota1 ?>" /></td>
            </tr>
            <tr>
                <td>Nota 2:</td>
                <td><input name="txtn2" type="text" value="<?= $nota2 ?>" /></td>
            </tr>
            <tr>
                <td>Nota 3:</td>
                <td><input name="txtn3" type="text" value="<?= $nota3 ?>" /></td>
            </tr>
            <tr>
                <td>Promedio:</td>
                <td><input name="txtp" type="text" value="<?= $promedio ?>" /></td>
            </tr>
            <tr>
                <td colspan="2"><input name="btnCalcular" type="submit" value="Calcular" /></td>
            </tr>
        </table>
    </form>
</body>
</html>