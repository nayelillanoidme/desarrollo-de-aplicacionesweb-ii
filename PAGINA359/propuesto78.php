
<!DOCTYPE html>
<html>
<head>
	<title>Invertir números</title>
	<style>
		body {
			font-family: Arial, sans-serif;
			background-color: #f2f2f2;
		}
		h1 {
			text-align:center ;
			color: #9370DB;
			margin-top: 30px;
		}
		form {
			margin: 0 auto;
			max-width: 200px;
			background-color: #fff;
			padding: 20px;
			border-radius: 5px;
			box-shadow: 0px 0px 10px #aaa;
		}
		label {
			display: ;
			margin-bottom: 10px;
			color: #333;
		}
		/input[type="text"] {
			padding: 10px;
			border-radius: 5px;
			border: none;
			box-shadow: 0px 0px 5px #aaa;
			margin-bottom: 20px;
		}
		button {
			background-color: #9370DB;
			color: #FF69B4;
			border: none;
			border-radius: 5px;
			padding: 10px 20px;
			cursor: ;
		}
		button:hover {
			background-color: #444;
		}
		#nombreInvertido {
			background-color: #DDA0DD;
			padding: 8px;
			border: ;
			box-shadow: 0px 0px 2px ;
			font-weight: ;
		}
	</style>
</head>
<body>
	<h1>Invertir números</h1>
	<form>
		<label for="nombre">Ingrese un número:</label>
		<input type="text" id="nombre" name="nombre"><br><br>
		<button type="button" onclick="invertirNombre()">Invertir número</button><br><br>
		<label for="nombreInvertido">Número invertido:</label>
		<input type="text" id="nombreInvertido" name="nombreInvertido" readonly>
	</form>
	<script>
        
		function invertirNombre() {
			let nombre = document.getElementById("nombre").value;
			let nombreInvertido = "";
			for (let i = nombre.length - 1; i >= 0; i--) {
				nombreInvertido += nombre.charAt(i);
			}
			document.getElementById("nombreInvertido").value = nombreInvertido;
		}
	</script>
</body>
</html>
