<?php
//Variables
$base = 0;
$altura = 0;
$area = 0;

if (isset($_POST["btnCalcular"])) {
    $base = (float)$_POST["txtBase"];
    $altura = (float)$_POST["txtAltura"];
    $area = $base * $altura;
}
?>

<html>

<head>
    <title>Calculadora de área de rectángulo</title>
    <style type="text/css">
        body {
            font-family: "Times New Roman",Courier,serif;
            background-color: #F0F8FF;
        }
        
        table {
            border-collapse: collapse;
            margin: 5 auto;
            background-color: #E0FFFF;
            border: 8px solid #DB7093 ;
            color: #7B68EE;
        }
        
        table td {
            border: 40px solid #FF69B4;
            padding: 15px;
        }
        
        table th {
            background: #E6E6FA;
            color: #663399;
            border: 15px solid #008080;
            padding: 15px;
        }
        
        .TextoFondo {
            background-color: #00CED1;
        }

    </style>
</head>

<body>
    <form method="post" action="propuesto76.php">
        <table width="241" border="0">
            <tr>
                <td colspan="2"><strong>Calculadora de área de rectángulo</strong> </td>
            </tr>
            <tr>
                <td width="81">Ingrese la base: </td>
                <td width="150">
                    <input name="txtBase" type="text" id="txtBase" value="<?= $base ?>" />
                </td>
            </tr>
            <tr>
                <td>Ingrese la altura: </td>
                <td>
                    <input name="txtAltura" type="text" id="txtAltura" value="<?= $altura ?>" />
                </td>
            </tr>
            <tr>
                <td>Área:</td>
                <td>
                    <input name="txtArea" type="text" class="TextoFondo" id="txtArea" value="<?= $area ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
