<?php
//Variables
$cifras = 0;
$divisor = 0;
$count = 0;

if(isset($_POST["btnCalcular"])) {
    $cifras = (int)$_POST["txtn1"];
    $divisor = (int)$_POST["txtn2"];

    $min = pow(10, $cifras-1);
    $max = pow(10, $cifras) - 1;
    $count =0;

    for($i = $min; $i <= $max ; $i++ ){
        if($i % $divisor == 0) {
            $count++;
        }
    }
}
?>

<html>

<head>
    <title>Número de múltiplos de un divisor</title>
    <style type="text/css">
        .TextoFondo {
            background-color: #CCFFFF;
        }
    </style>
</head>

<body>
    <form method="post" action="ejercicio6.php">
        <table width="241" border="0">
            <tr>
                <td colspan="2"><strong>Número de múltiplos de un divisor</strong> </td>
            </tr>
            <tr>
                <td width="81">Ingrese la cantidad de cifras: </td>
                <td width="150">
                    <input name="txtn1" type="text" id="txtn1" value="<?= $cifras ?>" />
                </td>
            </tr>
            <tr>
                <td width="81">Ingrese el divisor: </td>
                <td width="150">
                    <input name="txtn2" type="text" id="txtn2" value="<?= $divisor ?>" />
                </td>
            </tr>
            <tr>
                <td>Número de múltiplos:</td>
                <td>
                    <input name="txts" type="text" class="TextoFondo" id="txts" value="<?= $count ?>" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" />
                </td>
            </tr>
        </table>
    </form>
</body>

</html>
