<?php
// Variables
$n = 0;
$suma = 0;
$cantidad = 0;

if (isset($_POST["btnCalcular"])) {
    // Entrada
    $n = (int)$_POST["txtn"];

    // Proceso
    $fibonacci = array(0, 1);
    $suma = 1;
    $cantidad = 2;

    while ($fibonacci[count($fibonacci)-1] + $fibonacci[count($fibonacci)-2] < $n) {
        $fibonacci[] = $fibonacci[count($fibonacci)-1] + $fibonacci[count($fibonacci)-2];
        $suma += $fibonacci[count($fibonacci)-1];
        $cantidad++;
    }
}
?>

<html>
<head>
    <title>Suma y cantidad de números de la serie de Fibonacci menores a N</title>
    <style type="text/css">
        
    </style>
</head>
<body>
    <form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <table width="241" border="5">
            <tr>
                <td colspan="2"><strong>Suma y cantidad de números de la serie de Fibonacci menores a N</strong></td>
            </tr>
            <tr>
                <td width="81">Ingrese un número:</td>
                <td width="150"><input name="txtn" type="text" id="txtn" value="<?=$n?>" /></td>
            </tr>
            <tr>
                <td>Suma:</td>
                <td><input name="txts" type="text" class="TextoFondo" id="txts" value="<?=$suma?>" /></td>
            </tr>
            <tr>
                <td>Cantidad:</td>
                <td><input name="txtc" type="text" class="TextoFondo" id="txtc" value="<?=$cantidad?>" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" /></td>
            </tr>
        </table>
    </form>
</body>
</html>