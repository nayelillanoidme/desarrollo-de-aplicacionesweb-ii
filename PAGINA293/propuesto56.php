
<?php

$matriz = array(array(0, 0), array(0, 0), array(0, 0));

if (isset($_POST["btnCalcular"])) {
 
  $matriz[0][0] = $_POST["txt11"];
  $matriz[0][1] = $_POST["txt12"];
  $matriz[1][0] = $_POST["txt21"];
  $matriz[1][1] = $_POST["txt22"];
  $matriz[2][0] = $_POST["txt31"];
  $matriz[2][1] = $_POST["txt32"];

  // Calcular la suma de cada fila
  $sumaFila1 = $matriz[0][0] + $matriz[0][1];
  $sumaFila2 = $matriz[1][0] + $matriz[1][1];
  $sumaFila3 = $matriz[2][0] + $matriz[2][1];
}
?>

<html>
<head>
  <title>Suma de cada fila en matriz de 3x2</title>
  <style type="text/css">
     .TextoFondo {
            background-color: #CCFFFF;
        }

        body {
            font-family: "Times New Roman",Courier,serif;
            background-color:;
        }

        table {
            border-collapse: collapse;
            margin: 5 auto;
            background-color:;
            border:8 px solid #008080;
            color: #D8BFD8 ;
        }

        table td {
            border: 40px solid ;
            padding:15 px;
        }

        table th {
            background:#E6E6FA;
            color:#663399 ;
            border: 15px solid #008080;
            padding: 15px;
        }

        .TextoFondo {
            background-color:#00CED1;
        }

    
  </style>
</head>
<body>
  <form method="post" action="">
    <table>
      <tr>
        <td><input type="text" name="txt11" /></td>
        <td><input type="text" name="txt12" /></td>
      </tr>
      <tr>
        <td><input type="text" name="txt21" /></td>
        <td><input type="text" name="txt22" /></td>
      </tr>
      <tr>
        <td><input type="text" name="txt31" /></td>
        <td><input type="text" name="txt32" /></td>
      </tr>
      <tr>
        <td><strong>Suma fila 1:</strong></td>
        <td><input type="text" name="txtSuma1" value="<?= isset($sumaFila1) ? $sumaFila1 : '' ?>" /></td>
      </tr>
      <tr>
        <td><strong>Suma fila 2:</strong></td>
        <td><input type="text" name="txtSuma2" value="<?= isset($sumaFila2) ? $sumaFila2 : '' ?>" /></td>
      </tr>
      <tr>
        <td><strong>Suma fila 3:</strong></td>
        <td><input type="text" name="txtSuma3" value="<?= isset($sumaFila3) ? $sumaFila3 : '' ?>" /></td>
      </tr>
      <tr>
        <td colspan="2"><input type="submit" name="btnCalcular" value="Calcular" /></td>
      </tr>
    </table>
  </form>
</body>
</html>