<!DOCTYPE html>
<html>
<head>
	<title>Matriz 2x3</title>
</head>
<body>
	<form method="post">
		<label>Ingrese los números:</label><br>
		<input type="text" name="numeros[]" placeholder="Número 1"><br>
		<input type="text" name="numeros[]" placeholder="Número 2"><br>
		<input type="text" name="numeros[]" placeholder="Número 3"><br>
		<input type="text" name="numeros[]" placeholder="Número 4"><br>
		<input type="text" name="numeros[]" placeholder="Número 5"><br>
		<input type="text" name="numeros[]" placeholder="Número 6"><br>
		<label>Ingrese el valor K:</label><br>
		<input type="text" name="k"><br><br>
		<input type="submit" value="Calcular">
	</form>

	<?php
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$numeros = $_POST['numeros'];
			$k = $_POST['k'];
			$sum = 0;
			echo "<br>Matriz ingresada:<br>";
			echo $numeros[0]." ".$numeros[1]." ".$numeros[2]."<br>";
			echo $numeros[3]." ".$numeros[4]." ".$numeros[5]."<br>";
			echo "<br>Matriz resultante:<br>";
			for ($i=0; $i<count($numeros); $i++) {
				$numeros[$i] = $numeros[$i] * $k;
				echo $numeros[$i]." ";
				$sum += $numeros[$i];
				if (($i+1) % 3 == 0) {
					echo "<br>";
				}
			}
			echo "<br>La suma de los números de la matriz es ".$sum."<br>";
		}
	?>
</body>
</html>
